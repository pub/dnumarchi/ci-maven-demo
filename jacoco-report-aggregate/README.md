# Rapports de couverture

Ce module aggrège les rapports de couverture de code des autres modules de l'application.

Ceci permet, après conversion, d'afficher un rapport de couverture directement dans l'interface web de gitlab.

