package demo.ci.maven;

import demo.ci.maven.date.DateFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;

@RestController
public class EchoController {

    @Value("${app.version:??}")
    private String version;

    @GetMapping("/echo/{message}")
    public String echo(@PathVariable String message) {
        return (message == null ? "" : message);
    }

    @GetMapping(value = "/", produces = MediaType.TEXT_PLAIN_VALUE)
    public String index() {
        return String.format("Version: %s\nDate: %s", version, DateFormatter.toString(LocalDateTime.now()));
    }
}
