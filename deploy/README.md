# Déploiement de l'application

Ce dossier contient des exemples de configuration pour un déploiement sur Kubernetes.

Ces fichiers sont utilisés au travers du [.gitlab-ci.yml](../.gitlab-ci.yml ), dans les étapes de déploiement (phase `deploy`).

